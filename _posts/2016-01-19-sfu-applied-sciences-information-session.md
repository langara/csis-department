---
categories: []
layout: post
title: SFU Applied Sciences Information Session
created: 1453162361
---
<p>A representative from SFU will be at Langara on January 27 from 3:30-4:30pm in room C307.</p>
<p>Come learn about:</p>
<ul>
<li>School of Engineering Science</li>
<li>School of Mechatronic Systems Engineering</li>
<li>School of Computing Science</li>
</ul>
