---
categories: []
layout: post
title: Liquid Workshop Next Weekend (Nov 28)
created: 1447999634
---
<p>thinkific is holding a free Liquid dev workshop for students next weekend (Nov 28).</p>
<p>"The workshop will teach students and alumni about Liquid. Liquid is an open-source, Ruby-based template language created by Shopify. It is the backbone of Shopify themes and uses a combination of <em>tags</em>,<em>objects</em>, and<em>filters</em> to load dynamic content. It really helps programmers customize websites that can access dynamic data - to scratch the surface."</p>
<p>Details here: <a href="https://www.picatic.com/liquidworkshop">https://www.picatic.com/liquidworkshop</a></p>
