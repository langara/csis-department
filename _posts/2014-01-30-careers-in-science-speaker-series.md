---
categories: []
layout: post
title: Careers in Science Speaker Series
created: 1391060376
---
<p>Come hear James Hankle speak about <a href="http://www.langara.bc.ca/news-and-events/events-calendar/140131-careers-in-science.html">careers in computing science and software engineering</a>. Fri Jan 31 in A122a at 12:30pm. Complimentary lunch.</p>
