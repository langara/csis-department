---
categories: []
layout: post
title: CRTC Survey on Broadband Internet Usage
created: 1455212768
---
<p>The CRTC is conducting a survey on broadband internet usage:</p>
<p><a href="http://crtc.gc.ca/eng/internet/internet.htm?utm_source=embed&amp;utm_medium=rotator&amp;utm_campaign=talkbroadband"> <img src="http://crtc.gc.ca/images/650x275_broadband2-eng.jpg" alt="#TalkBroad: Have your say at crtc.gc.ca/talkbroadband/" /> </a></p>
