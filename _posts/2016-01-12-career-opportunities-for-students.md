---
categories:
- jobs
layout: post
title: Career Opportunities for Students
created: 1452620512
---
<p>For students interested in working the IT sector, you may be interested in attending the following events.</p>
<p>&nbsp;</p>
<p>TechFest January 19th from 6-9pm</p>
<p>https://www.picatic.com/BCTECHSummitTechFest</p>
<p>Open to all students eligible to work in Canada</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Communications Security Establishment Canada (CSEC)</p>
<p>https://www.cse-cst.gc.ca/en/careers-carrieres/students-etudiants/program-programme</p>
<p>Communications Security Establishment Canada (CSEC) will be on campus on February 1, 2016 from 12:00 to 1:00 pm in the Thinktank (C121g) to present information about their student job program - https://www.cse-cst.gc.ca/en/careers-carrieres/students-etudiants/program-programme. CSE offers more than 50 student placements per semester in a variety of disciplines.</p>
<p>They are currently recruiting for students in Mathematics, Computer Science, Engineering, and Web and Mobile Applications. Current Co-op students, as well as students who may enrol in Co-op in the future at Langara or at their transfer university, are encouraged to attend.&nbsp;</p>
<p>CSEC will provide pizza and beverages. To help them determine numbers, they would like students to register by January 25th. Email cbourcier@langara.ca to reserve a seat.</p>
<p>Note: You must be a Canadian Citizen to work for the Government of Canada.&nbsp;</p>
