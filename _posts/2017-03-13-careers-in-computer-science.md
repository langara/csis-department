---
categories: []
layout: post
title: Careers in Computer Science
created: 1489424063
---
<p style="outline: 0px; margin-top: 0px; margin-bottom: 15px; font-size: 13px; line-height: 17.94px; color: #333333; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;">Computer Science and technology is one of the fastest growing sectors in Vancouver. Come to this panel to hear from: Software Developer, Web Designer, Mobile App Designer and QA Tester.</p>
<p style="outline: 0px; margin-top: 0px; margin-bottom: 15px; font-size: 13px; line-height: 17.94px; color: #333333; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;">This is an opportunity to hear about their work, types of jobs and skills for these jobs including: development, design, testing. There will be a question and answer period for students to ask questions. Click on the names below to read more about these panelists.</p>
<p style="outline: 0px; margin-top: 0px; margin-bottom: 15px; font-size: 13px; line-height: 17.94px; color: #333333; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;">&nbsp;</p>
<p style="outline: 0px; margin-top: 0px; margin-bottom: 15px; font-size: 13px; line-height: 17.94px; color: #333333; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;">Panelists:</p>
<ul style="outline: 0px; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 16px;">
<li style="outline: 0px; font-size: 13px; line-height: 17.94px; color: #333333; margin-bottom: 5px; list-style: none; position: relative;"><a style="outline: 0px; background: transparent; transition: color 0.2s ease-in; color: #f15a22;" href="https://www.linkedin.com/in/juancho-buchanan-64213/">Juancho&nbsp;</a><a style="outline: 0px; background: transparent; transition: color 0.2s ease-in; color: #f15a22;" href="https://www.linkedin.com/in/juancho-buchanan-64213/">Buchanan,&nbsp;</a>Software Developer and Manager, Amazon Canada</li>
<li style="outline: 0px; font-size: 13px; line-height: 17.94px; color: #333333; margin-bottom: 5px; list-style: none; position: relative;">
<p style="outline: 0px; margin-top: 0px; margin-bottom: 15px; line-height: 17.94px;"><a style="outline: 0px; background: transparent; transition: color 0.2s ease-in; color: #f15a22;" href="https://www.linkedin.com/in/todd-desgagne-57613a43/" target="_blank">Todd Desgagne</a>, QA Project Lead, Electronic Arts</p>
</li>
<li style="outline: 0px; font-size: 13px; line-height: 17.94px; color: #333333; margin-bottom: 5px; list-style: none; position: relative;"><a style="outline: 0px; background: transparent; transition: color 0.2s ease-in; color: #f15a22;" href="https://www.linkedin.com/in/kianamohseni/" target="_blank">Kiana Mohseni</a>, Director of Product Development, HootSuite</li>
<li style="outline: 0px; font-size: 13px; line-height: 17.94px; color: #333333; margin-bottom: 5px; list-style: none; position: relative;"><a style="outline: 0px; background: transparent; transition: color 0.2s ease-in; color: #f15a22;" href="https://www.linkedin.com/in/gary-tong-84651926/">Gary Tong, BA&nbsp;&nbsp;</a>- Software Engineer, Function Point</li>
<li style="outline: 0px; font-size: 13px; line-height: 17.94px; color: #333333; margin-bottom: 5px; list-style: none; position: relative;">Mobify, Web/Mobile Application (speaker TBA)</li>
</ul>
<p>This event will be held in the T building gallery on Tuesday March 14 from 13:30-14:20.</p>
