---
categories: []
layout: post
title: MS Access Database Developer Needed
created: 1455243367
---
<p>&nbsp;</p>
<p><span style="font-family: arial, helvetica, sans-serif; font-size: small;">Forwarded on behalf of the VCH Public Health Surveillance Unit:&nbsp;</span></p>
<div class="page" title="Page 1">
<div class="layoutArea">
<div class="column">
<p><span style="font-size: small; font-family: arial,helvetica,sans-serif;">We are seeking a casual student developer to help develop an MS Access Database to support the work of a public health nursing team in managing routinely collected information on clients exposed to communicable diseases. </span></p>
<p>&nbsp;</p>
<div class="page" title="Page 1">
<div class="layoutArea">
<div class="column">
<p><span style="font-size: small; font-family: arial,helvetica,sans-serif;">The work will entail:</span></p>
<ul>
<li><span style="font-family: arial,helvetica,sans-serif; font-size: small;">Scoping and understanding technical requirements </span></li>
<li><span style="font-family: arial,helvetica,sans-serif; font-size: small;">Developing a project plan to identify deliverables and anticipated timeline </span></li>
<li><span style="font-family: arial,helvetica,sans-serif; font-size: small;">Understanding content and workflow in creating user interface (forms) </span></li>
<li><span style="font-family: arial,helvetica,sans-serif; font-size: small;">Seeking end‐user acceptability in development phase </span></li>
<li><span style="font-family: arial,helvetica,sans-serif; font-size: small;">Implementing database and considering mitigation of any potential risk during deployment </span>
<div class="page" title="Page 1">&nbsp;</div>
</li>
</ul>
<div class="page" title="Page 1">
<div class="layoutArea">
<div class="column"><span style="font-size: small; font-family: arial,helvetica,sans-serif;">The ideal candidate will have experience with creating MS Access databases and SQL programming.</span></div>
<div class="column">&nbsp;</div>
<div class="column"><span style="font-size: small; font-family: arial,helvetica,sans-serif;">The work can largely be undertaken remotely but will require meetings at our Vancouver office (Broadway corridor). </span></div>
<div class="column">&nbsp;</div>
<div class="column"><span style="font-family: arial,helvetica,sans-serif; font-size: small;">We anticipate 200‐300 hours of time needed over a 5‐month period. Compensation will be $25‐30hr depending on experience. </span></div>
<div class="column">
<div class="page" title="Page 1">
<div class="section" style="background-color: rgb(100.000000%, 100.000000%, 100.000000%);">
<div class="layoutArea">
<div class="column">
<p><span style="font-family: arial,helvetica,sans-serif; font-size: small;">If you feel you have the qualifications and are interested in applying for this position, please submit your resume and any examples of work demonstrating technical competence to <span style="color: #0000ff;">phsu@vch.ca </span>by February 15<span style="vertical-align: 6pt;">th </span>2016. </span></p>
<p>&nbsp;</p>
<div class="page" title="Page 1">
<div class="section" style="background-color: rgb(100.000000%, 100.000000%, 100.000000%);">
<div class="layoutArea">
<div class="column">
<p><span style="font-size: small; font-family: arial,helvetica,sans-serif;">We thank all candidates for their interest; however only those under consideration will be contacted. </span></p>
<div class="page" title="Page 1">
<div class="layoutArea">
<div class="column"><span style="font-size: small; font-family: arial,helvetica,sans-serif;">The successful candidate will have to agree to a confidentiality and privacy undertaking prior to commencing work. </span></div>
<div class="column">&nbsp;</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="column">&nbsp;</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<p>&nbsp;</p>
