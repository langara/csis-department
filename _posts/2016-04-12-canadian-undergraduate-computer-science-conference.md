---
categories: []
layout: post
title: Canadian Undergraduate Computer Science Conference
created: 1460493187
---
<p>When: June 22nd to 25th<br /> Where: BCIT Burnaby Campus<br /> How Much: $90 early-bird (ends May 15th), $120 regular price</p>
<p>Some key events include: a Women in Computer Science Dinner, an exclusive Career Fair, and social activities such as a Craft Beer Tour.</p>
<p>The central theme of this year's conference is applied research. In addition to speakers from Google, TELUS, and MDA; students are also encouraged to present on their own projects, research initiatives, or areas of interest.</p>
<p>Attached you will find the flyer and poster. For more information, please contact <a href="mailto:info@cucsc.ca">info@cucsc.ca</a> To see a comprehensive description of the speakers, sponsors, and events offered at the CUCSC 2016, please visit the website: <a href="http://www.cucsc.ca">www.cucsc.ca</a>.</p>
