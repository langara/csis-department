---
categories: []
layout: post
title: Seats are still available in summer courses
created: 1491331972
---
<p>There are still plenty of seats available in our Summer courses like CPSC 1030, 1045, 1050, and 1480. <a href="http://langara.ca/reg-guide/">Register now</a>!</p>
