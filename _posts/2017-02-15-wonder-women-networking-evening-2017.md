---
categories: []
layout: post
title: Wonder Women Networking Evening 2017
created: 1487184945
---
<p>The Society for Canadian Women in Science and Technology (SCWIST) and Science World are proud to present the annual <a href="https://www.eventbrite.ca/e/wonder-women-networking-evening-2017-tickets-31584444925?ref=ecal">Wonder Women Networking Evening</a>. Participants meet amazing women with careers in science and engineering and learn more about their pathways. This event will be held under the geodesic dome and is a joint venture between Science World and SCWIST.</p>
