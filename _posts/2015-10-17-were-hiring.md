---
categories: []
layout: post
title: We're Hiring!
created: 1445051113
---
<p>We are <a href="http://langara.ca/about-langara/employment-opportunities/faculty/instructor-computing-science-and-information-systems-f058-15.html">hiring faculty</a> for the Spring 2016 (Jan-Apr) semester.</p>
