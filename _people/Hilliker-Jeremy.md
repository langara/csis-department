---
name: Hilliker, Jeremy
title: Instructor
email: jhilliker@langara.ca
phone: 604.323.5511 ext. 2421
tel: "+16043235511,2421"
office: B019j
---
