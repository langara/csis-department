---
name: McKee-Scott, Jamie
title: Assistant Chair
email: jmckeescott@langara.ca
phone: 604.323.5511 ext. 2013
tel: "+16043235511,2013"
office: B019b
website: http://mylinux.langara.bc.ca/~jmckeescott/
---
