---
name: Green, Bryan
title: Department Chair
email: bgreen@langara.ca
phone: 604.323.5736
tel: "+16043235736"
office: B019h
website: http://mylinux.langara.bc.ca/~bgreen
---
